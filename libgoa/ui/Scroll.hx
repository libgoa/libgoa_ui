package libgoa.ui;

import hxd.Event;

class Scroll extends h2d.Flow {
	var pushed:Bool;
	var pushTime:Float = -1;
	var dragging:Bool;
	var scrollInertia:Float = 0;
	var scrollDir:Int;
	var containerContentDiff:Float;

	public var initialY(default, set):Float = 0;
	public var scrollHeight(default, set):Float = 0;
	public var clickTimeout:Float = .2;
	public var inertiaFriction:Float = .9;

	public function set_initialY(v:Float)
		return initialY = y = v;

	public function set_scrollHeight(v:Float)
		return scrollHeight = v;

	public function new(?parent) {
		super(parent);

		enableInteractive = true;
		verticalSpacing = 10;
		var pushY;
		
		interactive.onPush = function(e:Event)  {
			pushed = true;
			pushY = e.relY;
			containerContentDiff = scrollHeight - outerHeight;
			pushTime = haxe.Timer.stamp();
		}
		
		var previousY = 0.;
		interactive.onMove = function(e:Event) {
			if (pushed && !dragging && scrollHeight < outerHeight) {
				var startY = e.relY;
				interactive.startDrag(function(fe:Event) {
					y += fe.relY - startY;
					if (y > initialY)
						y = initialY;
					if (containerContentDiff < 0 && y < containerContentDiff)
						y = containerContentDiff;
					scrollInertia = hxd.Math.abs(previousY - startY);
					scrollDir = (previousY > startY) ? 1 : -1;
					previousY = fe.relY;
				});
				dragging = true;
			}
		}
		
		interactive.onWheel = function(e:Event) {
			if (y > initialY)
				y = initialY;
		}
		
		interactive.onRelease = function(e:Event) {
			if (dragging)
				interactive.stopDrag();
			pushed = dragging = false;
			var releaseTime = haxe.Timer.stamp();
			if (releaseTime - pushTime < clickTimeout && Math.abs(pushY - e.relY) < 5)
				onClick(e);
		}
	}
	
	public function onClick(e:Event) {
		var bounds = null, c, point = new h2d.col.Point(e.relX, e.relY);
		var i = children.length;
		while (i-- > 0) {
			c = children[i];
			if (c != null) {
				if (bounds == null)
					bounds = c.getSize();
				bounds.x = c.x;
				bounds.y = c.y;
				if (bounds.contains(point)) {
					onChildClicked(c);
					return;
				}
			}
		}
	}
	
	public function update() {
		if (!pushed && scrollInertia > 0) {
			y += scrollInertia * scrollDir;
			if (y > initialY) {
				y = initialY;
				scrollInertia = 0;
			}
			if (containerContentDiff < 0 && y < containerContentDiff) {
				y = containerContentDiff;
				scrollInertia = 0;
			}
			scrollInertia *= inertiaFriction;
			if (scrollInertia < .0001) scrollInertia = 0;
		}
		
	}
	
	public dynamic function onChildClicked(c:h2d.Object) {}
}
